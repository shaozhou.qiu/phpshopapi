EtSoftware Shop api

/*
*
*   扩展 1.0.0.0 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

此组件需运行在lnmp环境中。

仅供学习不得用于商业用途

安装，
    通过git仓库安装

    第一步，修改根目录下的 composer.json
        -----------------------------------------------------------------------
        "repositories": [
            {
                "type": "git",
                "url": "https://gitlab.com/shaozhou.qiu/phpshopapi.git",
                "reference":"master"
            }
        ],    
        "require": {
            "etsoftware/phpshopapi": "*"
        },
        "http-basic":{   
            "https://gitlab.com/shaozhou.qiu/phpshopapi.git":{   
                "username":"",   
                "password":""   
            }
        }    
        ----------------------------------------------------------------------- 
    第二步，更新依赖
        composer update
        composer dump-autoload 

使用：

<?php
    #ini_set('display_errors', 1);
    require __DIR__.'/../vendor/autoload.php';
    
    
?>